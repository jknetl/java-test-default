package com.etnetera.hr.data;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.List;
import java.util.Objects;

/**
 * Simple data entity describing basic properties of every JavaScript framework.
 * 
 * @author Etnetera
 *
 */
@Entity
public class JavaScriptFramework {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotEmpty
	@Column(nullable = false, length = 30)
	@Size(max = 30)
	private String name;

	@OneToMany(mappedBy = "framework", cascade = CascadeType.ALL)
	private List<Version> version;

	@Column(nullable = false)
	@NotNull
    @Min(0)
	@Max(10)
	private float hypeLevel;

	public JavaScriptFramework() {
	}

	public JavaScriptFramework(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Version> getVersion() {
		return version;
	}

	public void setVersion(List<Version> version) {
		this.version = version;
	}

	public float getHypeLevel() {
		return hypeLevel;
	}

	public void setHypeLevel(float hypeLevel) {
		this.hypeLevel = hypeLevel;
	}

	@Override
	public String toString() {
		return "JavaScriptFramework [id=" + id + ", name=" + name + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		JavaScriptFramework framework = (JavaScriptFramework) o;
		return Float.compare(framework.hypeLevel, hypeLevel) == 0 &&
				Objects.equals(id, framework.id) &&
				Objects.equals(name, framework.name) &&
				Objects.equals(version, framework.version);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name, version, hypeLevel);
	}
}
