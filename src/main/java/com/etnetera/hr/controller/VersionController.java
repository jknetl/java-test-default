package com.etnetera.hr.controller;

import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.data.Version;
import com.etnetera.hr.exception.FrameworkNotFoundException;
import com.etnetera.hr.exception.VersionNotFoundException;
import com.etnetera.hr.repository.JavaScriptFrameworkRepository;
import com.etnetera.hr.repository.VersionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
public class VersionController extends EtnRestController {


    private final JavaScriptFrameworkRepository frameworkRepo;
    private final VersionRepository versionRepo;

    @Autowired
    public VersionController(JavaScriptFrameworkRepository frameworkRepo, VersionRepository versionRepo) {
        this.frameworkRepo = frameworkRepo;
        this.versionRepo = versionRepo;
    }

    @GetMapping("/frameworks/{id}/versions")
    public Iterable<Version> versions(@PathVariable("id") Long frameworkId){
        JavaScriptFramework framework = frameworkRepo.findById(frameworkId)
                .orElseThrow(() -> new FrameworkNotFoundException(frameworkId));

        return versionRepo.findByFramework(framework);
    }

    @GetMapping("/frameworks/{fid}/versions/{vid}")
    public Version getVersion(@PathVariable("fid") Long frameworkId, @PathVariable("vid") Long versionId){
        JavaScriptFramework framework = frameworkRepo.findById(frameworkId)
                .orElseThrow(() -> new FrameworkNotFoundException(frameworkId));
        Version version = versionRepo.findByIdAndFramework(versionId,framework)
                .orElseThrow(() -> new VersionNotFoundException(versionId));



        return version;
    }

    @PostMapping("/frameworks/{id}/versions")
    public ResponseEntity<Version> addVersion(@PathVariable("id") Long frameworkId, @Valid @RequestBody Version version){
        JavaScriptFramework framework = frameworkRepo.findById(frameworkId)
                .orElseThrow(() -> new FrameworkNotFoundException(frameworkId));

        version.setFramework(framework);
        versionRepo.save(version);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(version.getId()).toUri();

        return ResponseEntity.created(location).build();
    }

    @PutMapping("/frameworks/{fid}/versions/{vid}")
    public ResponseEntity<Version> updateVersion(@PathVariable("fid") Long frameworkId,
                                              @PathVariable("vid") Long versionId,
                                              @Valid @RequestBody Version version){
        JavaScriptFramework framework = frameworkRepo.findById(frameworkId)
                .orElseThrow(() -> new FrameworkNotFoundException(frameworkId));
        Version existingVersion = versionRepo.findByIdAndFramework(versionId, framework)
                .orElseThrow(() -> new VersionNotFoundException(versionId));

        version.setId(versionId);
        version.setFramework(framework);
        versionRepo.save(version);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(version.getId()).toUri();

        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/frameworks/{fid}/versions/{vid}")
    public ResponseEntity<Version> deleteVersion(@PathVariable("fid") Long frameworkId,
                                              @PathVariable("vid") Long versionId){
        JavaScriptFramework framework = frameworkRepo.findById(frameworkId)
                .orElseThrow(() -> new FrameworkNotFoundException(frameworkId));
        Version version = versionRepo.findById(versionId)
                .orElseThrow(() -> new VersionNotFoundException(versionId));

        versionRepo.delete(version);

        return ResponseEntity.ok().build();
    }

}
