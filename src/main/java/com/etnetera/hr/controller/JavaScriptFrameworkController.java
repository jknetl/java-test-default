package com.etnetera.hr.controller;

import com.etnetera.hr.exception.FrameworkNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.repository.JavaScriptFrameworkRepository;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;


/**
 * Simple REST controller for accessing application logic.
 * 
 * @author Etnetera
 *
 */
@RestController
public class JavaScriptFrameworkController extends EtnRestController {

	private final JavaScriptFrameworkRepository repository;

	@Autowired
	public JavaScriptFrameworkController(JavaScriptFrameworkRepository repository) {
		this.repository = repository;
	}

	@GetMapping("/frameworks")
	public Iterable<JavaScriptFramework> frameworks() {
		return repository.findAll();
	}

	@GetMapping("/frameworks/{id}")
	public JavaScriptFramework framework(@PathVariable("id") Long id) {
		JavaScriptFramework existingFramework = repository.findById(id).
				orElseThrow(() -> new FrameworkNotFoundException(id));

		return existingFramework;
	}


	@PostMapping("/frameworks")
	public ResponseEntity<JavaScriptFramework> addFramework(@Valid @RequestBody JavaScriptFramework framework){

		repository.save(framework);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(framework.getId()).toUri();

		return ResponseEntity.created(location).build();
	}

	@PutMapping("/frameworks/{id}")
	public ResponseEntity<JavaScriptFramework> editFramework(@Valid @RequestBody JavaScriptFramework framework, @PathVariable("id") Long id) {
		JavaScriptFramework existingFramework = repository.findById(id).
				orElseThrow(() -> new FrameworkNotFoundException(id));

		framework.setId(id);
		repository.save(framework);

		return ResponseEntity.ok().build();
	}

	@DeleteMapping("/frameworks/{id}")
	public ResponseEntity<JavaScriptFramework> deleteFramework(@PathVariable("id") Long id) {
		JavaScriptFramework existingFramework = repository.findById(id).
				orElseThrow(() -> new FrameworkNotFoundException(id));

		repository.delete(existingFramework);

		return ResponseEntity.ok().build();
	}

	@GetMapping("/search")
	public Iterable<JavaScriptFramework> searchFrameworks( @RequestParam("q") String str,
														   @RequestParam(value = "ic", defaultValue = "true") boolean ignoreCase) {
	    if (ignoreCase) {
	    	return repository.findByNameIgnoringCaseContains(str);
		} else {
	    	return repository.findByNameContains(str);
		}
	}
}
