package com.etnetera.hr.rest;

/**
 * Represents an error in entity processing.
 *
 * @author Jakub Knetl
 */
public class EntityError {

    private String message;

    public EntityError(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
