package com.etnetera.hr.repository;

import org.springframework.data.repository.CrudRepository;

import com.etnetera.hr.data.JavaScriptFramework;

import java.util.List;

/**
 * Spring data repository interface used for accessing the data in database.
 * 
 * @author Etnetera
 *
 */
public interface JavaScriptFrameworkRepository extends CrudRepository<JavaScriptFramework, Long> {


    /**
     * Finds all frameworks which contains given string in its name
     * @param str pattern for searching
     * @return List of all frameworks which contains string str in its name
     */
    List<JavaScriptFramework> findByNameContains(String str);

    /**
     * Same as {@link #findByNameContains(String)} except this method is case insensitive.
     * @param str case insensitive pattern to search
     * @return List of all frameworks which contains the string in its name.
     */
    List<JavaScriptFramework> findByNameIgnoringCaseContains(String str);
}
