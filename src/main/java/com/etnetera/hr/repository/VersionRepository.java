package com.etnetera.hr.repository;

import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.data.Version;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface VersionRepository extends CrudRepository<Version, Long> {

    List<Version> findByFramework(JavaScriptFramework framework);
    Optional<Version> findByIdAndFramework(Long id, JavaScriptFramework framework);
}
