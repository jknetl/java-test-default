package com.etnetera.hr.exception;

public class FrameworkNotFoundException extends EntityException{
    public FrameworkNotFoundException(long frameworkId) {
        super("Cannot find JavaScript framework where id=" + frameworkId, frameworkId);
    }
}
