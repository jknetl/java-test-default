package com.etnetera.hr.exception;

public class EntityException extends RuntimeException{

    private Long id;

    public EntityException(String message, Long id) {
        super(message);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
