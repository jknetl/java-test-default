package com.etnetera.hr.exception;

public class VersionNotFoundException extends EntityException{
    public VersionNotFoundException(long versionId) {
        super("Cannot find framework version where id=" + versionId, versionId);
    }
}
