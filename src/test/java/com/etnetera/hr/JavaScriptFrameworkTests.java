package com.etnetera.hr;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.repository.JavaScriptFrameworkRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;


/**
 * Class used for Spring Boot/MVC based tests.
 * 
 * @author Etnetera
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class JavaScriptFrameworkTests {

	@Autowired
	private MockMvc mockMvc;
	
	private ObjectMapper mapper = new ObjectMapper();

	@Autowired
	private JavaScriptFrameworkRepository repository;

	private void prepareData() throws Exception {
		JavaScriptFramework react = new JavaScriptFramework("ReactJS");
		JavaScriptFramework vue = new JavaScriptFramework("Vue.js");

		repository.save(react);
		repository.save(vue);
	}

	@Test
	public void frameworksTest() throws Exception {
		prepareData();

		mockMvc.perform(get("/frameworks")).andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$", hasSize(2)))
				.andExpect(jsonPath("$[0].id", is(1)))
				.andExpect(jsonPath("$[0].name", is("ReactJS")))
				.andExpect(jsonPath("$[1].id", is(2)))
				.andExpect(jsonPath("$[1].name", is("Vue.js")));
	}

	@Test
	public void getFramework() throws Exception {
		JavaScriptFramework framework = TestUtils.createTestFramework();
		repository.save(framework);
		Long id = framework.getId();

		mockMvc.perform(get("/frameworks/" +id)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.name", is(framework.getName())))
				.andExpect(jsonPath("$.version", hasSize(framework.getVersion().size())));
	}

	@Test
	public void getFrameworkInvalid() throws Exception {
		mockMvc.perform(get("/frameworks/" +30)).andExpect(status().isNotFound());
	}

	@Test
	public void addFrameworkInvalid() throws JsonProcessingException, Exception {
		JavaScriptFramework framework = new JavaScriptFramework();
		mockMvc.perform(post("/frameworks").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsBytes(framework)))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.errors", hasSize(1)))
				.andExpect(jsonPath("$.errors[0].field", is("name")))
				.andExpect(jsonPath("$.errors[0].message", is("NotEmpty")));
		
		framework.setName("verylongnameofthejavascriptframeworkjavaisthebest");
		mockMvc.perform(post("/frameworks").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsBytes(framework)))
			.andExpect(status().isBadRequest())
			.andExpect(jsonPath("$.errors", hasSize(1)))
			.andExpect(jsonPath("$.errors[0].field", is("name")))
			.andExpect(jsonPath("$.errors[0].message", is("Size")));
		
	}

	@Test
    @Transactional
	public void addFramework() throws Exception {
		JavaScriptFramework framework = TestUtils.createTestFramework();

		ResultActions resultActions = mockMvc.perform(post("/frameworks").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsBytes(framework)));
		resultActions.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isCreated());

		// verify that framework really makes it into the database
		Optional<JavaScriptFramework> repositoryFramework = repository.findById(1L);
		Assert.assertTrue(repositoryFramework.isPresent());
		Assert.assertEquals(framework.getName(), repositoryFramework.get().getName());
		Assert.assertEquals(framework.getVersion().size(), repositoryFramework.get().getVersion().size());
		Assert.assertEquals(framework.getVersion().get(0).getDeprecationDate(), repositoryFramework.get().getVersion().get(0).getDeprecationDate());
	}

	@Test
	public void updateFrameworkInvalid() throws JsonProcessingException, Exception {
		JavaScriptFramework framework = TestUtils.createTestFramework();
		final long invalidId = 34;
		mockMvc.perform(put("/frameworks/" + invalidId).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsBytes(framework)))
				.andExpect(status().isNotFound())
				.andExpect(jsonPath("$.message", is("Cannot find JavaScript framework where id=34")));
	}

	@Test
	public void updateFramework() throws Exception {
		JavaScriptFramework framework = TestUtils.createTestFramework();
		repository.save(framework);
		long id = framework.getId();

		JavaScriptFramework updatedFramework = TestUtils.createTestFramework();
		String newName = "New name";
		updatedFramework.setName(newName);

		ResultActions resultActions = mockMvc.perform(put("/frameworks/" + id).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsBytes(updatedFramework)));
		resultActions.andDo(MockMvcResultHandlers.print())
				.andExpect(status().isOk());

		// verify that framework really makes it into the database
		Optional<JavaScriptFramework> repositoryFramework = repository.findById(1L);
		Assert.assertTrue(repositoryFramework.isPresent());
		Assert.assertEquals(newName, repositoryFramework.get().getName());
	}

	@Test
	public void deleteFramework() throws Exception {
		JavaScriptFramework framework = TestUtils.createTestFramework();
		repository.save(framework);
		long id = framework.getId();

		mockMvc.perform(delete("/frameworks/" + id))
				.andExpect(status().isOk());

		Assert.assertTrue(repository.findById(id).isEmpty());
	}
	@Test
	public void deleteFrameworkInvalid() throws Exception {
		mockMvc.perform(delete("/frameworks/" + 1))
				.andExpect(status().isNotFound());
	}

	@Test
	public void searchFrameworks() throws Exception {
		prepareData();

		mockMvc.perform(get("/search").param("q", "none"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(0)));

		mockMvc.perform(get("/search").param("q", "js"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(2)));

		mockMvc.perform(get("/search").param("q", "js").param("ic", "false"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(1)));
	}

	@Test
	public void searchFrameworksInvalid() throws Exception {
		mockMvc.perform(get("/search"))
				.andExpect(status().isBadRequest());
	}

}
