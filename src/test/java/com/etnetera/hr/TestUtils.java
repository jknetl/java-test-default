package com.etnetera.hr;

import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.data.Version;

import java.time.LocalDate;
import java.util.Arrays;

public final class TestUtils {

    private TestUtils() {
    }


    public static JavaScriptFramework createTestFramework() {

        JavaScriptFramework framework = new JavaScriptFramework();
        framework.setName("Angular");
        framework.setHypeLevel(5);

        Version angular7 = new Version();
        angular7.setDeprecationDate(LocalDate.of(2020,4,18));
        angular7.setVersion("7.0.0");
        angular7.setFramework(framework);
        framework.setVersion(Arrays.asList(angular7));

        return framework;
    }
}
