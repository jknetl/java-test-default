package com.etnetera.hr;


import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.data.Version;
import com.etnetera.hr.repository.JavaScriptFrameworkRepository;
import com.etnetera.hr.repository.VersionRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class JavaScriptVersionTest {

    @Autowired
    private MockMvc mockMvc;

    private ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private JavaScriptFrameworkRepository frameworkRepo;

    @Autowired
    private VersionRepository versionRepo;


    @Test
    public void versionsTest() throws Exception {
        JavaScriptFramework framework = TestUtils.createTestFramework();

        frameworkRepo.save(framework);

        mockMvc.perform(get("/frameworks/" + framework.getId() + "/versions"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].version", is("7.0.0")));
    }

    @Test
    public void getVersionsTest() throws Exception {
        JavaScriptFramework framework = TestUtils.createTestFramework();

        frameworkRepo.save(framework);
        Long versionId = framework.getVersion().get(0).getId();

        mockMvc.perform(get("/frameworks/" + framework.getId() + "/versions/" + versionId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.version", is("7.0.0")));
    }

    @Test
    public void getVersionsInvalidTest() throws Exception {
        JavaScriptFramework framework = TestUtils.createTestFramework();

        frameworkRepo.save(framework);
        Long versionId = framework.getVersion().get(0).getId();

        mockMvc.perform(get("/frameworks/" + framework.getId() + "/versions/" + 999))
                .andExpect(status().isNotFound());

        mockMvc.perform(get("/frameworks/" + 999 + "/versions/" + versionId))
                .andExpect(status().isNotFound());
    }

    @Test
    public void addVersionTest() throws Exception {
        JavaScriptFramework framework = TestUtils.createTestFramework();

        frameworkRepo.save(framework);
        Long versionId = framework.getVersion().get(0).getId();

        Version version8 = new Version();
        version8.setVersion("8.2.3");
        version8.setDeprecationDate(LocalDate.of(2021,10,5));

        mockMvc.perform(post("/frameworks/" + framework.getId() + "/versions")
                .contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsBytes(version8)))
                .andExpect(status().isCreated());

        Assert.assertEquals(versionRepo.findByFramework(framework).size(), 2);
        Assert.assertEquals(versionRepo.findByFramework(framework).get(1).getVersion(), version8.getVersion());
    }

    @Test
    public void updateVersionTest() throws Exception {
        JavaScriptFramework framework = TestUtils.createTestFramework();

        frameworkRepo.save(framework);
        Long versionId = framework.getVersion().get(0).getId();

        Version version8 = new Version();
        version8.setVersion("8.2.3");
        version8.setDeprecationDate(LocalDate.of(2021,10,5));

        mockMvc.perform(put("/frameworks/" + framework.getId() + "/versions/" + versionId )
                .contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsBytes(version8)))
                .andExpect(status().isOk());

        Assert.assertEquals(versionRepo.findByFramework(framework).size(), 1);
        Assert.assertEquals(versionRepo.findByFramework(framework).get(0).getVersion(), version8.getVersion());
    }

    @Test
    public void deleteVersionTest() throws  Exception {
        JavaScriptFramework framework = TestUtils.createTestFramework();

        frameworkRepo.save(framework);
        Long versionId = framework.getVersion().get(0).getId();

        mockMvc.perform(delete("/frameworks/" + framework.getId() + "/versions/" + versionId ))
                .andExpect(status().isOk());

        Assert.assertEquals(versionRepo.findByFramework(framework).size(), 0);
    }
}
